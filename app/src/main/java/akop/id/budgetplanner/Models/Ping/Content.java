package akop.id.budgetplanner.Models.Ping;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Content {

    @SerializedName("login")
    @Expose
    private Boolean login;
    @SerializedName("subscriptions")
    @Expose
    private List<String> subscriptions = null;
    @SerializedName("group_count")
    @Expose
    private Integer groupCount;
    @SerializedName("user")
    @Expose
    private User user;

    public Boolean getLogin() {
        return login;
    }

    public void setLogin(Boolean login) {
        this.login = login;
    }

    public List<String> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(List<String> subscriptions) {
        this.subscriptions = subscriptions;
    }

    public Integer getGroupCount() {
        return groupCount;
    }

    public void setGroupCount(Integer groupCount) {
        this.groupCount = groupCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}