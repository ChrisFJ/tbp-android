package akop.id.budgetplanner.Models.CreateGroup;

public class CreateRequest {

    final String name;
    final String description;

    public CreateRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
