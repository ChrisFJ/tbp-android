package akop.id.budgetplanner.Models.Transaction.Create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTransactionResponse {
    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("msg")
    @Expose
    private Object msg;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }
}
