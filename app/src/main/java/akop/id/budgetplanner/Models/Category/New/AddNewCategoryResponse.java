package akop.id.budgetplanner.Models.Category.New;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddNewCategoryResponse {
    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("msg")
    @Expose
    private Object msg;
    @SerializedName("content")
    @Expose
    private Content content;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }
}
