package akop.id.budgetplanner.Models.Pay;

public class CreatePay {

    private final String nonce;
    private final String plan_id;

    public CreatePay(String nonce, String plan_id) {
        this.nonce = nonce;
        this.plan_id = plan_id;
    }

}
