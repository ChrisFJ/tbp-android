package akop.id.budgetplanner.Models.TableView;

public class ColumnHeader extends Cell {
    public ColumnHeader(String id) {
        super(id);
    }

    public ColumnHeader(String id, String data) {
        super(id, data);
    }
}
