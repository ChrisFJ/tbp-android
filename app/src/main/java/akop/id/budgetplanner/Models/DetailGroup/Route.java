package akop.id.budgetplanner.Models.DetailGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {

    @SerializedName("(subscription|transaction|group|currency|confirm|session)(/(?P<id>\\w+))?")
    @Expose
    private String subscriptionTransactionGroupCurrencyConfirmSessionPIdW;

    public String getSubscriptionTransactionGroupCurrencyConfirmSessionPIdW() {
        return subscriptionTransactionGroupCurrencyConfirmSessionPIdW;
    }

    public void setSubscriptionTransactionGroupCurrencyConfirmSessionPIdW(String subscriptionTransactionGroupCurrencyConfirmSessionPIdW) {
        this.subscriptionTransactionGroupCurrencyConfirmSessionPIdW = subscriptionTransactionGroupCurrencyConfirmSessionPIdW;
    }

}