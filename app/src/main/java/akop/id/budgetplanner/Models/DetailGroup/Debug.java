package akop.id.budgetplanner.Models.DetailGroup;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Debug {

    @SerializedName("method")
    @Expose
    private String method;
    @SerializedName("route")
    @Expose
    private Route route;
    @SerializedName("handler")
    @Expose
    private String handler;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public String getHandler() {
        return handler;
    }

    public void setHandler(String handler) {
        this.handler = handler;
    }

}