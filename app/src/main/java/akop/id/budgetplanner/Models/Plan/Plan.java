package akop.id.budgetplanner.Models.Plan;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Plan {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("merchantId")
    @Expose
    private String merchantId;
    @SerializedName("billingDayOfMonth")
    @Expose
    private Object billingDayOfMonth;
    @SerializedName("billingFrequency")
    @Expose
    private Integer billingFrequency;
    @SerializedName("currencyIsoCode")
    @Expose
    private String currencyIsoCode;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("numberOfBillingCycles")
    @Expose
    private Object numberOfBillingCycles;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("trialDuration")
    @Expose
    private Object trialDuration;
    @SerializedName("trialDurationUnit")
    @Expose
    private Object trialDurationUnit;
    @SerializedName("trialPeriod")
    @Expose
    private Boolean trialPeriod;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("addOns")
    @Expose
    private List<Object> addOns = null;
    @SerializedName("discounts")
    @Expose
    private List<Object> discounts = null;
    @SerializedName("plans")
    @Expose
    private List<Object> plans = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Object getBillingDayOfMonth() {
        return billingDayOfMonth;
    }

    public void setBillingDayOfMonth(Object billingDayOfMonth) {
        this.billingDayOfMonth = billingDayOfMonth;
    }

    public Integer getBillingFrequency() {
        return billingFrequency;
    }

    public void setBillingFrequency(Integer billingFrequency) {
        this.billingFrequency = billingFrequency;
    }

    public String getCurrencyIsoCode() {
        return currencyIsoCode;
    }

    public void setCurrencyIsoCode(String currencyIsoCode) {
        this.currencyIsoCode = currencyIsoCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getNumberOfBillingCycles() {
        return numberOfBillingCycles;
    }

    public void setNumberOfBillingCycles(Object numberOfBillingCycles) {
        this.numberOfBillingCycles = numberOfBillingCycles;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Object getTrialDuration() {
        return trialDuration;
    }

    public void setTrialDuration(Object trialDuration) {
        this.trialDuration = trialDuration;
    }

    public Object getTrialDurationUnit() {
        return trialDurationUnit;
    }

    public void setTrialDurationUnit(Object trialDurationUnit) {
        this.trialDurationUnit = trialDurationUnit;
    }

    public Boolean getTrialPeriod() {
        return trialPeriod;
    }

    public void setTrialPeriod(Boolean trialPeriod) {
        this.trialPeriod = trialPeriod;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Object> getAddOns() {
        return addOns;
    }

    public void setAddOns(List<Object> addOns) {
        this.addOns = addOns;
    }

    public List<Object> getDiscounts() {
        return discounts;
    }

    public void setDiscounts(List<Object> discounts) {
        this.discounts = discounts;
    }

    public List<Object> getPlans() {
        return plans;
    }

    public void setPlans(List<Object> plans) {
        this.plans = plans;
    }

}
