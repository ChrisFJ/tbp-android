package akop.id.budgetplanner.Models.CreateWallet;

public class CreateWallet {

    private final String name;
    private final int group_id;
    private final int currency_id;
    private final String description;

    public CreateWallet(String name,int group_id, int currency_id, String description) {
        this.name = name;
        this.group_id = group_id;
        this.currency_id = currency_id;
        this.description = description;
    }
}
