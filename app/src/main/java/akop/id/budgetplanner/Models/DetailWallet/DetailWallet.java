package akop.id.budgetplanner.Models.DetailWallet;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import akop.id.budgetplanner.Models.DetailGroup.Debug;

public class DetailWallet {

    @SerializedName("ok")
    @Expose
    private Boolean ok;
    @SerializedName("msg")
    @Expose
    private Object msg;
    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("debug")
    @Expose
    private Debug debug;

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Debug getDebug() {
        return debug;
    }

    public void setDebug(Debug debug) {
        this.debug = debug;
    }

}