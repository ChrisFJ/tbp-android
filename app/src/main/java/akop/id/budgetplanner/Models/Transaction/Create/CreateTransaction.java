package akop.id.budgetplanner.Models.Transaction.Create;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreateTransaction {

    final int wallet_category_id;
    final int amount;
    final String memo;
    final String  status;
    final int wallet_id;
    final int type;
    final String[] attachments;

    public CreateTransaction(int wallet_category_id,int amount, String memo, String status, int wallet_id, int type, String[] attachments) {
        this.wallet_category_id = wallet_category_id;
        this.amount = amount;
        this.memo = memo;
        this.status = status;
        this.wallet_id = wallet_id;
        this.type = type;
        this.attachments = attachments;
    }
}