package akop.id.budgetplanner.Models.DetailWallet;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Info {

    @SerializedName("starting")
    @Expose
    private String starting;
    @SerializedName("spending")
    @Expose
    private String spending;
    @SerializedName("income")
    @Expose
    private String income;
    @SerializedName("balance")
    @Expose
    private String balance;

    public String getStarting() {
        return starting;
    }

    public void setStarting(String starting) {
        this.starting = starting;
    }

    public String getSpending() {
        return spending;
    }

    public void setSpending(String spending) {
        this.spending = spending;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

}