package akop.id.budgetplanner.Models.Category.New;

public class CreateCategory {

    final String name;
    final int initial_amount;
    final String description;

    public CreateCategory(String name,int initial_amount, String description) {
        this.name = name;
        this.initial_amount = initial_amount;
        this.description = description;
    }
}
