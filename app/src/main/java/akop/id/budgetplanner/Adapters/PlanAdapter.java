package akop.id.budgetplanner.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import akop.id.budgetplanner.Interfaces.PlanDelegate;
import akop.id.budgetplanner.Models.Plan.Plan;
import akop.id.budgetplanner.R;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PlanAdapter extends RecyclerView.Adapter<PlanAdapter.viewHolder> {

    private List<Plan> plans;
    private PlanDelegate delegate;

    public PlanAdapter(PlanDelegate planDelegate, List<Plan> planList) {
        this.plans = planList;
        this.delegate = planDelegate;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_plan, parent, false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, final int position) {
        holder.bind(plans.get(position));
        holder.cardGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delegate.onPlanGetTapped(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return plans.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_plan_title)
        TextView textViewTitle;
        @BindView(R.id.tv_plan_desc)
        TextView textViewDesc;
        @BindView(R.id.tv_plan_price)
        TextView textViewPrice;
        @BindView(R.id.card_get)
        CardView cardGet;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Plan plan) {
            textViewTitle.setText(plan.getName());
            textViewDesc.setText(plan.getDescription());
            textViewPrice.setText(plan.getPrice());
        }

    }
}
