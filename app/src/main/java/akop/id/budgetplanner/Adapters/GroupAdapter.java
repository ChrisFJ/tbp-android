package akop.id.budgetplanner.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import akop.id.budgetplanner.Interfaces.GroupDelegate;
import akop.id.budgetplanner.Models.List.Content;
import akop.id.budgetplanner.R;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class GroupAdapter extends RecyclerView.Adapter<GroupAdapter.GroupVH> {

    private List<Content> contentList;
    private GroupDelegate groupDelegate;

    public GroupAdapter(List<Content> contents, GroupDelegate delegate) {
        this.contentList = contents;
        this.groupDelegate = delegate;
    }

    @NonNull
    @Override
    public GroupVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_item_layout, parent, false);
        return new GroupVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GroupVH holder, final int position) {
        holder.bind(contentList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                groupDelegate.didGroupSelectedAt(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contentList.size();
    }

    public class GroupVH extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_group_name)
        public AppCompatTextView textViewGroupName;

        GroupVH(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(Content content) {
            textViewGroupName.setText(content.getName());
        }

    }
}
