package akop.id.budgetplanner.Adapters;

import akop.id.budgetplanner.Views.Fragments.InvitedFragment;
import akop.id.budgetplanner.Views.Fragments.MyGroupFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0)
        {
            fragment = new MyGroupFragment();
        }
        else if (position == 1)
        {
            fragment = new InvitedFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0)
        {
            title = "My Group";
        }
        else if (position == 1)
        {
            title = "Invited Group";
        }
        return title;
    }
}
