package akop.id.budgetplanner.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import akop.id.budgetplanner.Helpers.GBPHelper;
import akop.id.budgetplanner.Helpers.TBPHelper;
import akop.id.budgetplanner.Interfaces.WalletDelegate;
import akop.id.budgetplanner.Models.DetailGroup.Wallet;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Views.Activities.GBPActivityNewCategory;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {

    private List<Wallet> walletList;
    private WalletDelegate walletDelegate;
    private Context mContext;

    public WalletAdapter(Context context, WalletDelegate delegate, List<Wallet> wallets) {
        this.walletList = wallets;
        this.walletDelegate = delegate;
        this.mContext = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_wallet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.bind(walletList.get(position));
        holder.layoutDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletDelegate.didWalletSelectedAt(position);
            }
        });
        holder.layoutAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                walletDelegate.didAddCategoryTapped(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return walletList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_wallet_name)
        TextView textViewWalletName;
        @BindView(R.id.tv_wallet_value)
        TextView textViewWalletValue;
        @BindView(R.id.tv_wallet_desc)
        TextView textViewWalletDesc;
        @BindView(R.id.layout_add)
        LinearLayout layoutAdd;
        @BindView(R.id.layout_wallet_detail)
        LinearLayout layoutDetail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Wallet wallet) {

            this.textViewWalletName.setText(wallet.getName());
            this.textViewWalletValue.setText(GBPHelper.formatCurrency(wallet.getCurrency().getSymbol(),wallet.getBalance()));
            this.textViewWalletDesc.setText(wallet.getDescription());
        }
    }

}
