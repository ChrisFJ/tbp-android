package akop.id.budgetplanner.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dynamitechetan.flowinggradient.FlowingGradientClass;
import com.google.android.material.card.MaterialCardView;

import org.w3c.dom.Text;

import java.util.List;

import akop.id.budgetplanner.Models.Members.Content;
import akop.id.budgetplanner.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MembersAdapter extends RecyclerView.Adapter<MembersAdapter.ViewHolder> {

    private List<Content> members;

    public MembersAdapter(List<Content> contents) {
        this.members = contents;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_member_horizontal, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(members.get(position));
    }

    @Override
    public int getItemCount() {
        return members.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_name_char)
        TextView textViewName;
        @BindView(R.id.tv_full_name)
        TextView textViewFullName;
        @BindView(R.id.member_rl_card)
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Content member) {
            char charAtBeggining = member.getUsername().charAt(0);
            this.textViewName.setText(String.valueOf(charAtBeggining).toUpperCase());
            textViewFullName.setText(member.getUsername());

            FlowingGradientClass grad = new FlowingGradientClass();
            grad.setBackgroundResource(R.drawable.translate)
                    .onRelativeLayout(relativeLayout)
                    .setTransitionDuration(4000)
                    .start();
        }
    }

}
