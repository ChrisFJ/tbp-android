package akop.id.budgetplanner.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import akop.id.budgetplanner.Helpers.GBPHelper;
import akop.id.budgetplanner.Models.Transaction.Content;
import akop.id.budgetplanner.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    private List<Content> transactionList;
    private Context mContext;
    public String symbol;

    public TransactionAdapter(Context context, String currencySymbol, List<Content> transactions) {
        this.mContext = context;
        this.symbol = currencySymbol;
        this.transactionList = transactions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(transactionList.get(position));
    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_transaction_memo)
        TextView textViewMemo;
        @BindView(R.id.tv_transaction_title)
        TextView textViewTitle;
        @BindView(R.id.tv_transaction_amount)
        TextView textViewAmount;
        @BindView(R.id.tv_transaction_date_day)
        TextView textViewDateDay;
        @BindView(R.id.tv_transaction_day)
        TextView textViewDayName;
        @BindView(R.id.tv_transaction_time)
        TextView textViewTime;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Content transaction) {
            SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            try {
                Date dtIn = inFormat.parse(transaction.getCreated());
                SimpleDateFormat localDateFormat = new SimpleDateFormat("HH:mm");
                Calendar cal = Calendar.getInstance();
                cal.setTime(dtIn);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                String dayOfWeek = new SimpleDateFormat("MMM yy", Locale.ENGLISH).format(dtIn);
                String time = localDateFormat.format(dtIn);

                textViewTime.setText(time);
                textViewDateDay.setText(String.valueOf(day));
                textViewDayName.setText(dayOfWeek);

            } catch (ParseException e) {
                e.printStackTrace();
            }
            this.textViewMemo.setText(transaction.getMemo());
            this.textViewTitle.setText(transaction.getUsername());
            this.textViewAmount.setText(GBPHelper.formatCurrency(symbol, transaction.getAmount()));
            //this.textViewCategory.setText(GBPHelper.statusconfirmed(Boolean.parseBoolean(transaction.getStatus())));
            if (transaction.getMemo().length() > 0) {
                textViewMemo.setVisibility(View.VISIBLE);
            }
        }
    }
}
