package akop.id.budgetplanner.Adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import akop.id.budgetplanner.Helpers.GBPHelper;
import akop.id.budgetplanner.Helpers.TBPHelper;
import akop.id.budgetplanner.Models.DetailGroup.Balance;
import akop.id.budgetplanner.R;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class BalancesAdapter extends RecyclerView.Adapter<BalancesAdapter.ViewHolder> {

    private List<Balance> balanceList;

    public BalancesAdapter(List<Balance> balances){
        this.balanceList = balances;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_balance, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(balanceList.get(position));
    }

    @Override
    public int getItemCount() {
        return balanceList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.tv_balance_name)
        TextView textViewBalanceName;
        @BindView(R.id.tv_balance_value)
        TextView textViewBalanceValue;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("SetTextI18n")
        public void bind(Balance balance){
            this.textViewBalanceName.setText(balance.getCode());
            this.textViewBalanceValue.setText(balance.getSymbol()+" "+GBPHelper.currencyFormatter(String.valueOf(balance.getBalance())));
        }
    }
}
