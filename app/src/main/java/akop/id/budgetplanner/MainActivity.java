package akop.id.budgetplanner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.pixplicity.easyprefs.library.Prefs;

import akop.id.budgetplanner.Adapters.ViewPagerAdapter;
import akop.id.budgetplanner.Interfaces.InvitedBundleDelegate;
import akop.id.budgetplanner.Interfaces.MainBundleDelegate;
import akop.id.budgetplanner.Models.List.GetListResponse;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.Activities.CreateGroupActivity;
import akop.id.budgetplanner.Views.Activities.ProfileActivity;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.profile)
    CircleImageView imageView;

    MainBundleDelegate bundleDelegate;
    InvitedBundleDelegate invitedBundleDelegate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setToolbar();
        setToolbarHomeTitle("GBP");

        Glide.with(this).load(Prefs.getString("pp", "")).into(imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateGroupActivity.class);
                startActivity(intent);
            }
        });

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        String token = Prefs.getString("token", "");
        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);
        Call<GetListResponse> call = gbpapi.getGroups(token);
        call.enqueue(new Callback<GetListResponse>() {
            @Override
            public void onResponse(Call<GetListResponse> call, Response<GetListResponse> response) {
                if (response.body().getOk()) {
                    Log.e("TAG", response.body().getContent().size()+"");
                    bundleDelegate.onBundleReady(response.body().getContent());
                    invitedBundleDelegate.onBundleReady(response.body().getContent());
                }
            }
            @Override
            public void onFailure(Call<GetListResponse> call, Throwable t) {
            }
        });
        inRequestAdds();
    }

    public void setBundleDelegate(MainBundleDelegate bundleDelegate) {
        this.bundleDelegate = bundleDelegate;
    }

    public void setInvitedBundleDelegate(InvitedBundleDelegate invitedBundleDelegate) {
        this.invitedBundleDelegate = invitedBundleDelegate;
    }
}
