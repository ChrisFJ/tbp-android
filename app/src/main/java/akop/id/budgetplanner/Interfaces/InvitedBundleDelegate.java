package akop.id.budgetplanner.Interfaces;

import java.util.List;

import akop.id.budgetplanner.Models.List.Content;

public interface InvitedBundleDelegate {
    void onBundleReady(List<Content> contents);
}
