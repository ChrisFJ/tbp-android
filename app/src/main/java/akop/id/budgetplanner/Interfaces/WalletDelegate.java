package akop.id.budgetplanner.Interfaces;

public interface WalletDelegate {

    void didWalletSelectedAt(int index);
    void didRemoveTappedAt(int index);
    void didAddCategoryTapped(int index);
}
