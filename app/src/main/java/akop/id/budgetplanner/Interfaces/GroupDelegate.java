package akop.id.budgetplanner.Interfaces;

public interface GroupDelegate {

    void didGroupSelectedAt(int index);
    void didGroupDeletedAt(int index);

}
