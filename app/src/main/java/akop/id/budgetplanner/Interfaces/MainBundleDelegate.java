package akop.id.budgetplanner.Interfaces;

import android.os.Bundle;

import java.util.List;

import akop.id.budgetplanner.Models.List.Content;

public interface MainBundleDelegate {
    void onBundleReady(List<Content> contents);
}
