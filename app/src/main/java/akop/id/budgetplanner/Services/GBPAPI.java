package akop.id.budgetplanner.Services;

import akop.id.budgetplanner.Models.Category.New.AddNewCategoryResponse;
import akop.id.budgetplanner.Models.Category.New.CreateCategory;
import akop.id.budgetplanner.Models.Category.list.GetListCResponse;
import akop.id.budgetplanner.Models.CreateGroup.CreateRequest;
import akop.id.budgetplanner.Models.CreateGroup.CreateResponse;
import akop.id.budgetplanner.Models.CreateWallet.CreateWallet;
import akop.id.budgetplanner.Models.CreateWallet.CreateWalletResponse;
import akop.id.budgetplanner.Models.DetailGroup.GetGroupResponse;
import akop.id.budgetplanner.Models.DetailWallet.DetailWallet;
import akop.id.budgetplanner.Models.List.GetListResponse;
import akop.id.budgetplanner.Models.Login.LoginResponse;
import akop.id.budgetplanner.Models.Members.MembersResponse;
import akop.id.budgetplanner.Models.Pay.CreatePay;
import akop.id.budgetplanner.Models.Pay.PostPay;
import akop.id.budgetplanner.Models.Payment.Token.Token;
import akop.id.budgetplanner.Models.Ping.PingResponse;
import akop.id.budgetplanner.Models.Plan.GetPlan;
import akop.id.budgetplanner.Models.Transaction.Create.CreateTransaction;
import akop.id.budgetplanner.Models.Transaction.Create.CreateTransactionResponse;
import akop.id.budgetplanner.Models.Transaction.TransactionResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GBPAPI {

    @FormUrlEncoded
    @POST("/auth/google")
    Call<LoginResponse> getUserToken(@Field("code") String code, @Field("redirect_uri") String redirect);

    @Headers({
            "Content-Type: application/json"
    })
    @PUT("/group")
    Call<CreateResponse> putNewGroup(@Header("X-Token") String token, @Body CreateRequest request);

    @GET("/group/list")
    Call<GetListResponse> getGroups(@Header("X-Token") String token);

    @Headers("Content-Type: application/json")
    @GET("/group/{group_id}")
    Call<GetGroupResponse> getGroupDetail(@Header("X-Token") String token, @Path(value = "group_id", encoded = true) String userId);

    @Headers("Content-Type: application/json")
    @GET("/group/{group_id}/member")
    Call<MembersResponse> getGroupMembers(@Header("X-Token") String token, @Path(value = "group_id", encoded = true) String userId);

    @Headers("Content-Type: application/json")
    @GET("/group/{group_id}/wallet/{wallet_id}")
    Call<DetailWallet> getWallet(@Header("X-Token") String token, @Path(value = "group_id", encoded = true) String group_id, @Path(value = "wallet_id", encoded = true) int walletId);

    @Headers("Content-Type: application/json")
    @GET("/wallet/{wallet_id}")
    Call<DetailWallet> getWalletDetail(@Header("X-Token") String token, @Path(value = "wallet_id", encoded = true) int walletId);


    @Headers({
            "Content-Type: application/json"
    })
    @PUT("/wallet/{wallet_id}/category")
    Call<AddNewCategoryResponse> putCategory(@Header("X-Token") String token, @Path(value = "wallet_id", encoded = true) int wallet_id, @Body CreateCategory createCategory);

    @Headers("Content-Type: application/json")
    @GET("/wallet/{wallet_id}/transaction")
    Call<TransactionResponse> getTransactions(@Header("X-Token") String token, @Path(value = "wallet_id", encoded = true) int wallet_id);

    @Headers("Content-Type: application/json")
    @PUT("/wallet")
    Call<CreateWalletResponse> putWallet(@Header("X-Token") String token, @Body CreateWallet request);

    @Headers("Content-Type: application/json")
    @GET("/ping")
    Call<PingResponse> ping(@Header("X-Token") String token);

    @Headers({
            "Content-Type: application/json"
    })
    @GET("/wallet/{wallet_id}/category/list")
    Call<GetListCResponse> getCategoryList(@Header("X-Token") String token, @Path(value = "wallet_id", encoded = true) int wallet_id);

    @Headers("Content-Type: application/json")
    @PUT("/transaction/{wallet_id}")
    Call<CreateTransactionResponse> putTransaction(@Header("X-Token") String token, @Path(value = "wallet_id", encoded = true) int wallet_id,@Body CreateTransaction request);

    @Headers("Content-Type: application/json")
    @GET("/payment/client-token")
    Call<Token> getPaymentToken(@Header("X-Token") String token);

    @Headers("Content-Type: application/json")
    @GET("/plan")
    Call<GetPlan> getPlan(@Header("X-Token") String token);

    @Headers("Content-Type: application/json")
    @PUT("payment/pay")
    Call<PostPay> postPay(@Header("X-Token") String token, @Body CreatePay request);
}
