package akop.id.budgetplanner;

import android.app.Application;
import android.content.ContextWrapper;

import com.pixplicity.easyprefs.library.Prefs;

import akop.id.budgetplanner.Services.GBPAPI;

public class App extends Application {

    private static GBPAPI sApiClient;

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize the Prefs class
        new Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(getPackageName())
                .setUseDefaultSharedPreference(true)
                .build();
    }

    public static void resetApiClient() {
        sApiClient = null;
    }
}
