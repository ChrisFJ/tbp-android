package akop.id.budgetplanner.Views.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import akop.id.budgetplanner.Adapters.GroupAdapter;
import akop.id.budgetplanner.Interfaces.GroupDelegate;
import akop.id.budgetplanner.Interfaces.InvitedBundleDelegate;
import akop.id.budgetplanner.Interfaces.MainBundleDelegate;
import akop.id.budgetplanner.MainActivity;
import akop.id.budgetplanner.Models.List.Content;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Views.Activities.WalletActivity;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.dynamicbox.DynamicBox;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvitedFragment extends Fragment implements InvitedBundleDelegate, GroupDelegate {

    private MainActivity mActivity;
    private List<Content> contentList = new ArrayList<>();

    public InvitedFragment() {}

    @BindView(R.id.rv_group)
    RecyclerView recyclerView;

    private GroupAdapter adapter;
    DynamicBox box;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_invited, container, false);
        ButterKnife.bind(this, view);
        box = new DynamicBox(getActivity(),recyclerView); // or new DynamicBox(this,R.id.listView)
        box.setLoadingMessage("Loading your groups ...");

        View emptyCollectionView = getLayoutInflater().inflate(R.layout.gbp_empty_group, null, false);
        box.addCustomView(emptyCollectionView,"music_not_found");
        box.showLoadingLayout();
        return view;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (MainActivity) getActivity();
        mActivity.setInvitedBundleDelegate(this);
    }
    @Override
    public void onBundleReady(List<Content> contents) {
        for (Content item : contents) {
            if (!item.getRole().equals("Owner")) {
                contentList.add(item);
            }
        }
        if (contentList.size() > 0) {

            new Handler().postDelayed(new Runnable() {
                public void run() {
                    adapter = new GroupAdapter(contentList, InvitedFragment.this);

                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);
                    box.hideAll();
                }
            }, 1000);
        } else {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    adapter = new GroupAdapter(contentList, InvitedFragment.this);

                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                    recyclerView.setAdapter(adapter);
                    box.showCustomView("music_not_found");
                }
            }, 1000);
        }
}

    @Override
    public void didGroupSelectedAt(int index) {
        Intent i = new Intent(getActivity(), WalletActivity.class);
        i.putExtra("id", contentList.get(index).getId());
        i.putExtra("groupName", contentList.get(index).getName());
        i.putExtra("role", contentList.get(index).getRole());
        startActivity(i);
    }

    @Override
    public void didGroupDeletedAt(int index) {

    }
}
