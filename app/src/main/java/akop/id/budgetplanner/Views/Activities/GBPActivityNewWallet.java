package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Models.CreateWallet.CreateWallet;
import akop.id.budgetplanner.Models.CreateWallet.CreateWalletResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;

public class GBPActivityNewWallet extends BaseActivity {

    @BindView(R.id.editTextWalletCurency)
    TextInputEditText editTextCurrency;
    @BindView(R.id.editTextWalletName)
    TextInputEditText editTextName;
    @BindView(R.id.editTextWalletDescription)
    TextInputEditText editTextDescription;
    @BindView(R.id.buttonWalletSubmit)
    Button buttonWalletSubmit;

    private int selectedCurrencyID = 0;
    private int groupID;
    CFAlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gbpnew_wallet);
        ButterKnife.bind(this);

        setToolbar();
        showBackArrow();
        setToolbarTitle("New Wallet");
        groupID = getIntent().getIntExtra("groupID", 0);

        setupUI();
        buttonWalletSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String token = Prefs.getString("token", "");
                GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);
                String name = editTextName.getText().toString();
                String desc = editTextDescription.getText().toString();
                CreateWallet data = new CreateWallet(name, groupID, selectedCurrencyID, desc);
                Call<CreateWalletResponse> call = gbpapi.putWallet(token, data);
                call.enqueue(new Callback<CreateWalletResponse>() {
                    @Override
                    public void onResponse(Call<CreateWalletResponse> call, Response<CreateWalletResponse> response) {
                        if (response.isSuccessful()) {
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<CreateWalletResponse> call, Throwable t) {

                    }
                });
            }
        });
    }

    private void setupUI() {
        editTextCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(GBPActivityNewWallet.this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET);
                builder.setTitle("Currency");
                builder.setMessage("Select your wallet currency");
                builder.addButton("IDR", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedCurrencyID = 1;
                        editTextCurrency.setText("IDR");
                        alertDialog.dismiss();
                    }
                });
                builder.addButton("USD", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selectedCurrencyID = 2;
                        editTextCurrency.setText("USD");
                        alertDialog.dismiss();
                    }
                });
                builder.setCancelable(true);
                alertDialog = builder.show();
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                    }
                });
            }
        });
    }
}
