package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.pixplicity.easyprefs.library.Prefs;

public class ProfileActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.profile)
    CircleImageView imageView;
    @BindView(R.id.tvProfileName)
    TextView textViewName;
    @BindView(R.id.card_profile_upgrade)
    MaterialCardView cardUpgrade;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setToolbar();
        showBackArrow();
        setToolbarTitle("Profile");

        Glide.with(this).load(Prefs.getString("pp", "")).into(imageView);
        textViewName.setText(Prefs.getString("fullname","Username"));

        cardUpgrade.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.card_profile_upgrade:
                Intent intentUpgrade = new Intent(this, GBPActivityPackageOption.class);
                startActivity(intentUpgrade);
                break;
        }
    }
}
