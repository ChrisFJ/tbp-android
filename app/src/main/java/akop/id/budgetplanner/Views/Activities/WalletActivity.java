package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Adapters.BalancesAdapter;
import akop.id.budgetplanner.Adapters.MembersAdapter;
import akop.id.budgetplanner.Adapters.WalletAdapter;
import akop.id.budgetplanner.Interfaces.WalletDelegate;
import akop.id.budgetplanner.MainActivity;
import akop.id.budgetplanner.Models.DetailGroup.Balance;
import akop.id.budgetplanner.Models.DetailGroup.GetGroupResponse;
import akop.id.budgetplanner.Models.DetailGroup.Wallet;
import akop.id.budgetplanner.Models.Members.Content;
import akop.id.budgetplanner.Models.Members.MembersResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.dynamicbox.DynamicBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pixplicity.easyprefs.library.Prefs;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class WalletActivity extends BaseActivity implements WalletDelegate {

    @BindView(R.id.tv_wallet_groupName)
    TextView textViewGroupName;
    @BindView(R.id.tv_description)
    TextView textViewDesction;
    @BindView(R.id.rv_balances)
    RecyclerView recyclerViewBalance;
    @BindView(R.id.rv_wallets)
    RecyclerView recyclerViewWallets;
    @BindView(R.id.rv_members)
    RecyclerView recyclerViewMembers;
    @BindView(R.id.imageview_you)
    ImageView imageViewYou;
    @BindView(R.id.nested_group)
    NestedScrollView nestedScrollView;
    @BindView(R.id.cardAddWallet)
    MaterialCardView cardAddWAllet;
    @BindView(R.id.layout_wallet_add_wallet)
    RelativeLayout layoutAddWallet;

    private BalancesAdapter balancesAdapter;
    private WalletAdapter walletAdapter;

    private List<Balance> balances;
    private List<Wallet> wallets;
    private List<Content> contentsMember;

    private DynamicBox box;
    private DynamicBox boxWallets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        final String id = getIntent().getStringExtra("id");

        setToolbar();
        textViewGroupName.setText(getIntent().getStringExtra("groupName"));
        setToolbarTitle("");
        showBackArrow();

        cardAddWAllet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WalletActivity.this, GBPActivityNewWallet.class);
                intent.putExtra("groupID", Integer.parseInt(id));
                startActivity(intent);
            }
        });
        Glide.with(this).load(Prefs.getString("pp", "")).into(imageViewYou);
        imageViewYou.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        boxWallets = new DynamicBox(this, recyclerViewWallets);
        View emptyWallet = getLayoutInflater().inflate(R.layout.gbp_empty_wallet, null, false);
        boxWallets.addCustomView(emptyWallet, "wallet_not_found");
        boxWallets.showLoadingLayout();

        box = new DynamicBox(this, recyclerViewBalance); // or new DynamicBox(this,R.id.listView)
        View emptyBalance = getLayoutInflater().inflate(R.layout.gbp_empty_balance, null, false);
        box.addCustomView(emptyBalance, "balance_not_found");
        box.showLoadingLayout();

        String token = Prefs.getString("token", "");
        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);



        Call<GetGroupResponse> call = gbpapi.getGroupDetail(token, id);
        call.enqueue(new Callback<GetGroupResponse>() {
            @Override
            public void onResponse(Call<GetGroupResponse> call, Response<GetGroupResponse> response) {
                if (response.isSuccessful()) {
                    textViewDesction.setText(response.body().getContent().getDescription());
                    balances = response.body().getContent().getBalances();

                    balancesAdapter = new BalancesAdapter(balances);
                    recyclerViewBalance.setLayoutManager(new LinearLayoutManager(WalletActivity.this));
                    recyclerViewBalance.setNestedScrollingEnabled(true);
                    recyclerViewBalance.setAdapter(balancesAdapter);

                    if (balances.size() > 0) {
                        box.hideAll();
                        boxWallets.hideAll();
                    } else {
                        box.showCustomView("balance_not_found");
                        boxWallets.hideAll();
                    }

                    wallets = response.body().getContent().getWallets();
                    if (wallets.size() < 3) {
                        layoutAddWallet.setVisibility(View.GONE);
                    }
                    recyclerViewWallets.setLayoutManager(new LinearLayoutManager(WalletActivity.this));
                    walletAdapter = new WalletAdapter(WalletActivity.this,WalletActivity.this,wallets);
                    recyclerViewWallets.setNestedScrollingEnabled(false);
                    recyclerViewWallets.setAdapter(walletAdapter);
                }
            }

            @Override
            public void onFailure(Call<GetGroupResponse> call, Throwable t) {

            }
        });
        layoutAddWallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WalletActivity.this, GBPActivityNewWallet.class);
                intent.putExtra("groupID", Integer.parseInt(id));
                startActivity(intent);
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerViewMembers.setLayoutManager(layoutManager);

        final Call<MembersResponse> callMembers = gbpapi.getGroupMembers(token, id);
        callMembers.enqueue(new Callback<MembersResponse>() {
            @Override
            public void onResponse(Call<MembersResponse> call, Response<MembersResponse> response) {
                if (response.isSuccessful()){
                    contentsMember = response.body().getContent();
                    for (int i=0; i<contentsMember.size()-1; i++){
                        if (contentsMember.get(i).getMemberId().equals(Prefs.getInt("id", 0))){
                            contentsMember.remove(i);
                        }
                    }
                    recyclerViewMembers.setAdapter(new MembersAdapter(contentsMember));
                }
            }

            @Override
            public void onFailure(Call<MembersResponse> call, Throwable t) {

            }
        });
        if (!getIntent().getStringExtra("role").equals("Owner")) {
            cardAddWAllet.setVisibility(View.GONE);
            layoutAddWallet.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void didWalletSelectedAt(int index) {
        int idWallet = wallets.get(index).getId();
        String idGroup = getIntent().getStringExtra("id");

        Intent intent = new Intent(WalletActivity.this, WalletDetailActivity.class);
        intent.putExtra("idWallet", idWallet);
        intent.putExtra("idGroup", idGroup);
        startActivity(intent);
    }

    @Override
    public void didRemoveTappedAt(int index) {

    }

    @Override
    public void didAddCategoryTapped(int index) {
        int idWallet = wallets.get(index).getId();

        Intent intent = new Intent(WalletActivity.this, GBPActivityNewCategory.class);
        intent.putExtra("idWallet", idWallet);
        startActivity(intent);
    }
}
