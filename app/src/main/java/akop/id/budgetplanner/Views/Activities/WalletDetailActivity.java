package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Adapters.TransactionAdapter;
import akop.id.budgetplanner.Models.DetailGroup.GetGroupResponse;
import akop.id.budgetplanner.Models.DetailWallet.DetailWallet;
import akop.id.budgetplanner.Models.TableView.Cell;
import akop.id.budgetplanner.Models.TableView.ColumnHeader;
import akop.id.budgetplanner.Models.TableView.RowHeader;
import akop.id.budgetplanner.Models.Transaction.Content;
import akop.id.budgetplanner.Models.Transaction.TransactionResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.evrencoskun.tableview.TableView;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.List;

public class WalletDetailActivity extends BaseActivity {

    @BindView(R.id.tv_wallet_desc)
    TextView textViewWalletDesc;
    @BindView(R.id.tv_wallet_start_date)
    TextView textViewWalletStartDate;
    @BindView(R.id.tv_wallet_start_balance)
    TextView textViewWalletStartBalance;
    @BindView(R.id.tv_wallet_spending)
    TextView textViewWalletSpending;
    @BindView(R.id.tv_wallet_income)
    TextView textViewWalletIncome;
    @BindView(R.id.tv_wallet_balance)
    TextView textViewWalletBalance;
    @BindView(R.id.tv_wallet_desc_name)
    TextView textViewWalletName;
    @BindView(R.id.rv_transactions)
    RecyclerView recyclerViewTransactions;

    private akop.id.budgetplanner.Models.DetailWallet.Content detailWallet;
    private List<Content> contentTransactions;
    private TransactionAdapter transactionAdapter;
    private GBPAPI gbpapi;
    private String token;
    private  int idWallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet_detail);
        ButterKnife.bind(this);
        setToolbar();
        showBackArrow();
        setToolbarTitle("");

        recyclerViewTransactions.setLayoutManager(new LinearLayoutManager(this));

        idWallet = getIntent().getIntExtra("idWallet", 0);
        String idGroup = getIntent().getStringExtra("idGroup");

        token = Prefs.getString("token", "");
        gbpapi = APIClient.getClient().create(GBPAPI.class);

        Call<DetailWallet> call = gbpapi.getWallet(token, idGroup, idWallet);
        call.enqueue(new Callback<DetailWallet>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<DetailWallet> call, Response<DetailWallet> response) {
                detailWallet = response.body().getContent();
                textViewWalletName.setText(response.body().getContent().getName());
                textViewWalletDesc.setText(detailWallet.getDescription());
                textViewWalletDesc.setText(detailWallet.getDescription());
                textViewWalletStartDate.setText(detailWallet.getCreated());
                textViewWalletStartBalance.setText(detailWallet
                        .getCurrency()
                        .getSymbol() + " " +
                        currencyFormatter(subsValue(detailWallet
                                .getInfo()
                                .getStarting())));
                textViewWalletSpending.setText(detailWallet
                        .getCurrency()
                        .getSymbol() + " " +
                        currencyFormatter(subsValue(detailWallet
                                .getInfo()
                                .getSpending())));
                textViewWalletIncome.setText(detailWallet
                        .getCurrency()
                        .getSymbol() + " " +
                        currencyFormatter(subsValue(detailWallet
                                .getInfo()
                                .getIncome())));
                textViewWalletBalance.setText(detailWallet
                        .getCurrency()
                        .getSymbol() + " " +
                        currencyFormatter(subsValue(detailWallet
                                .getInfo()
                                .getBalance())));
                Call<TransactionResponse> responseCall = gbpapi.getTransactions(token, idWallet);
                responseCall.enqueue(new Callback<TransactionResponse>() {
                    @Override
                    public void onResponse(Call<TransactionResponse> call, Response<TransactionResponse> response) {
                        if (response.isSuccessful()) {
                            contentTransactions = response.body().getContent();
                            transactionAdapter = new TransactionAdapter(WalletDetailActivity.this,detailWallet.getCurrency().getSymbol(), contentTransactions);
                            recyclerViewTransactions.setAdapter(transactionAdapter);
                            hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(Call<TransactionResponse> call, Throwable t) {

                    }
                });
            }

            @Override
            public void onFailure(Call<DetailWallet> call, Throwable t) {
                hideDialog();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuAdd:
                Intent intentAddTransaction = new Intent(WalletDetailActivity.this,GBPActivityNewTransaction.class);
                intentAddTransaction.putExtra("walletID", idWallet);
                startActivity(intentAddTransaction);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add, menu);

        // return true so that the menu pop up is opened
        return true;
    }
}
