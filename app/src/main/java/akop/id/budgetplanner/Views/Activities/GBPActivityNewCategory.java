package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Models.Category.New.AddNewCategoryResponse;
import akop.id.budgetplanner.Models.Category.New.CreateCategory;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.pixplicity.easyprefs.library.Prefs;

public class GBPActivityNewCategory extends BaseActivity {

    @BindView(R.id.editTextCategoryName)
    TextInputEditText editTextName;
    @BindView(R.id.editTextCategoryInitial)
    TextInputEditText editTextInitial;
    @BindView(R.id.editTextCategoryDescription)
    TextInputEditText editTextDescription;
    @BindView(R.id.buttonCategorySubmit)
    MaterialButton buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gbpnew_category);
        ButterKnife.bind(this);
        setToolbar();
        showBackArrow();
        setToolbarTitle("New Category");
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }

    private void submit() {
        int idWallet = getIntent().getIntExtra("idWallet", 0);
        String token = Prefs.getString("token", "");
        String name = editTextName.getText().toString();
        int init = Integer.parseInt(editTextInitial.getText().toString());
        String desc = editTextDescription.getText().toString();
        CreateCategory category = new CreateCategory(name, init, desc);
        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);



        Call<AddNewCategoryResponse> addNewCategoryResponseCall = gbpapi.putCategory(token,idWallet, category);
        addNewCategoryResponseCall.enqueue(new Callback<AddNewCategoryResponse>() {
            @Override
            public void onResponse(Call<AddNewCategoryResponse> call, Response<AddNewCategoryResponse> response) {
                if (response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<AddNewCategoryResponse> call, Throwable t) {

            }
        });
    }
}
