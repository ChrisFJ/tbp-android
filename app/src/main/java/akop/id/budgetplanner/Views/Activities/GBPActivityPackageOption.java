package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Adapters.PlanAdapter;
import akop.id.budgetplanner.Helpers.BraintreeSettings;
import akop.id.budgetplanner.Interfaces.PlanDelegate;
import akop.id.budgetplanner.Models.Pay.CreatePay;
import akop.id.budgetplanner.Models.Pay.PostPay;
import akop.id.budgetplanner.Models.Payment.Token.Token;
import akop.id.budgetplanner.Models.Plan.GetPlan;
import akop.id.budgetplanner.Models.Plan.Plan;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import mehdi.sakout.dynamicbox.DynamicBox;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.braintreepayments.api.PayPal;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.GooglePaymentRequest;
import com.google.android.gms.wallet.Cart;
import com.google.android.gms.wallet.LineItem;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.WalletConstants;
import com.google.android.material.card.MaterialCardView;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GBPActivityPackageOption extends BaseActivity implements PlanDelegate {

    @BindView(R.id.rv_plans)
    RecyclerView recyclerViewPlans;

    private static final int DROP_IN_REQUEST = 2;
    private static String SANDBOX_TOKENIZATION_KEY = "sandbox_csghg24r_cqh9kzr55nh66cny";
    private List<Plan> plans = new ArrayList<>();
    private DynamicBox box;
    private String selectedPlanID = "";
    String token = Prefs.getString("token", "");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gbppackage_option);
        ButterKnife.bind(this);
        setToolbar();
        setToolbarTitle("Subscribe");
        showBackArrow();

        box = new DynamicBox(this, recyclerViewPlans); // or new DynamicBox(this,R.id.listView)
        View emptyBalance = getLayoutInflater().inflate(R.layout.gbp_empty_plan, null, false);
        box.addCustomView(emptyBalance, "plans_not_found");
        box.setLoadingMessage("we are preparing the best package for you...");
        box.showLoadingLayout();

        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);
        Call<Token> call = gbpapi.getPaymentToken(token);
        final Call<GetPlan> callPlan = gbpapi.getPlan(token);

        call.enqueue(new Callback<Token>() {
            @Override
            public void onResponse(Call<Token> call, Response<Token> response) {
                SANDBOX_TOKENIZATION_KEY = response.body().getContent().getClientToken();
                callPlan.enqueue(new Callback<GetPlan>() {
                    @Override
                    public void onResponse(Call<GetPlan> call, Response<GetPlan> response) {
                        if (response.isSuccessful()) {
                            plans = response.body().getContent().getPlans();
                            recyclerViewPlans.setLayoutManager(new LinearLayoutManager(GBPActivityPackageOption.this));
                            recyclerViewPlans.setAdapter(new PlanAdapter(GBPActivityPackageOption.this,plans));
                            box.hideAll();
                        } else {
                            showAlertDialog(response.message());
                        }
                    }

                    @Override
                    public void onFailure(Call<GetPlan> call, Throwable t) {
                        showAlertDialog(t.getLocalizedMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<Token> call, Throwable t) {

            }
        });
    }
    public void onBraintreeSubmit(String amount) {
        DropInRequest dropInRequest = new DropInRequest()
                .clientToken(SANDBOX_TOKENIZATION_KEY)
                .amount(amount)
                .requestThreeDSecureVerification(BraintreeSettings.isThreeDSecureEnabled(this))
                .collectDeviceData(BraintreeSettings.shouldCollectDeviceData(this))
                .googlePaymentRequest(getGooglePaymentRequest())
                .maskCardNumber(true)
                .maskSecurityCode(true)
                .androidPayCart(getAndroidPayCart())
                .androidPayShippingAddressRequired(BraintreeSettings.isAndroidPayShippingAddressRequired(this))
                .androidPayPhoneNumberRequired(BraintreeSettings.isAndroidPayPhoneNumberRequired(this))
                .androidPayAllowedCountriesForShipping(BraintreeSettings.getAndroidPayAllowedCountriesForShipping(this))
                .vaultManager(BraintreeSettings.isVaultManagerEnabled(this))
                .cardholderNameStatus(BraintreeSettings.getCardholderNameStatus(this));

        if (BraintreeSettings.isPayPalAddressScopeRequested(this)) {
            dropInRequest.paypalAdditionalScopes(Collections.singletonList(PayPal.SCOPE_ADDRESS));
        }
        startActivityForResult(dropInRequest.getIntent(this), DROP_IN_REQUEST);
    }

    private GooglePaymentRequest getGooglePaymentRequest() {
        return new GooglePaymentRequest()
                .transactionInfo(TransactionInfo.newBuilder()
                        .setTotalPrice("1.00")
                        .setCurrencyCode("USD")
                        .setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL)
                        .build())
                .emailRequired(true);
    }

    private Cart getAndroidPayCart() {
        return Cart.newBuilder()
                .setCurrencyCode(BraintreeSettings.getAndroidPayCurrency(this))
                .setTotalPrice("1.00")
                .addLineItem(LineItem.newBuilder()
                        .setCurrencyCode("USD")
                        .setDescription("Description")
                        .setQuantity("1")
                        .setUnitPrice("1.00")
                        .setTotalPrice("1.00")
                        .build())
                .build();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == DROP_IN_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                showProgressDialog("Payment","in progress...");
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String paymentMethodNonce = result.getPaymentMethodNonce().getNonce();
                // send paymentMethodNonce to your server
                putPay(paymentMethodNonce);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // canceled
            } else {
                // an error occurred, checked the returned exception
                Exception exception = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                showAlertDialog(exception.getMessage());
            }
        }
    }
    private void putPay(String nonce) {
        CreatePay pay = new CreatePay(nonce, selectedPlanID);
        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);
        Call<PostPay> payCall = gbpapi.postPay(token, pay);
        payCall.enqueue(new Callback<PostPay>() {
            @Override
            public void onResponse(Call<PostPay> call, Response<PostPay> response) {
                if (response.isSuccessful()){
                    hideDialog();
                    finish();
                }
            }
            @Override
            public void onFailure(Call<PostPay> call, Throwable t) {

            }
        });
    }
    @Override
    public void onPlanGetTapped(int index) {
        selectedPlanID = plans.get(index).getId();
        onBraintreeSubmit(plans.get(index).getPrice());
    }
}
