package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Models.CreateGroup.CreateRequest;
import akop.id.budgetplanner.Models.CreateGroup.CreateResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.google.android.material.textfield.TextInputEditText;
import com.pixplicity.easyprefs.library.Prefs;

public class CreateGroupActivity extends BaseActivity {

    @BindView(R.id.edit_text_group_name)
    TextInputEditText editTextGroupName;
    @BindView(R.id.edit_text_group_description)
    TextInputEditText editTextGroupDesctiption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        setToolbar();
        setToolbarTitle("New Group");
        showBackArrow();
    }

    @OnClick(R.id.button_group_save) void saveNewGroup(){
        String token = Prefs.getString("token", "");
        GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);

        CreateRequest request = new CreateRequest(editTextGroupName.getText().toString(), editTextGroupDesctiption.getText().toString());
        Call<CreateResponse> call = gbpapi.putNewGroup(token, request);

        call.enqueue(new Callback<CreateResponse>() {
            @Override
            public void onResponse(Call<CreateResponse> call, Response<CreateResponse> response) {
                if (response.isSuccessful()){
                    finish();
                }
            }

            @Override
            public void onFailure(Call<CreateResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
