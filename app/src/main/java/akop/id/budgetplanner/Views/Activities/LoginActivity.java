package akop.id.budgetplanner.Views.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.pixplicity.easyprefs.library.Prefs;

import akop.id.budgetplanner.MainActivity;
import akop.id.budgetplanner.Models.Login.LoginResponse;
import akop.id.budgetplanner.Models.Ping.PingResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private static final int RC_SIGN_IN = 0xff;
    private static final String CLIENT_ID="83414597563-pge7h9o7q28hfli1ambbqk5mmemuemnp.apps.googleusercontent.com";
    @BindView(R.id.sign_in_button)
    SignInButton signInButton;

    private GoogleSignInClient mGoogleSignInClient;
    private String TAG = LoginActivity.class.getSimpleName();
    private GBPAPI gbpapi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestServerAuthCode(CLIENT_ID)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        gbpapi = APIClient.getClient().create(GBPAPI.class);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                final GoogleSignInAccount account = task.getResult(ApiException.class);
                assert account != null;
                String authCode = account.getServerAuthCode();
                Log.d(TAG, "AUTHCODE: " + authCode);

                // API Login
                Call<LoginResponse> call = gbpapi.getUserToken(authCode, "urn:ietf:wg:oauth:2.0:oob");
                call.enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<LoginResponse> call, @NonNull Response<LoginResponse> response) {
                        assert response.body() != null;
                        final String serverToken = response.body().getContent().getToken();
                        Call<PingResponse> pingResponseCall = gbpapi.ping(serverToken);
                        pingResponseCall.enqueue(new Callback<PingResponse>() {
                            @Override
                            public void onResponse(Call<PingResponse> call, Response<PingResponse> response) {
                                Prefs.putInt("id", response.body().getContent().getUser().getId());
                                Prefs.putString("username", response.body().getContent().getUser().getUsername());
                                Prefs.putString("fullname", response.body().getContent().getUser().getName());
                                Prefs.putString("token", serverToken);
                                Prefs.putString("pp", account.getPhotoUrl().toString());

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);
                                finish();
                            }
                            @Override
                            public void onFailure(Call<PingResponse> call, Throwable t) {

                            }
                        });
                    }

                    @Override
                    public void onFailure(@NonNull Call<LoginResponse> call, @NonNull Throwable t) {
                    }
                });

            } catch (ApiException e) {
                Log.e(TAG, e.getMessage());

                Log.w(TAG, "Sign-in failed", e);
                String errMsg = "Unknown error, make sure you have Google Play Services";
                if( e.getStatusCode() == 12500 ){
                    errMsg = "You Google Play Service is outdated.";
                }
            }
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
//            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
//            updateUI(null);
        }
    }
}
