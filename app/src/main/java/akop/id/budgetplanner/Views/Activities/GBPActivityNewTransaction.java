package akop.id.budgetplanner.Views.Activities;

import akop.id.budgetplanner.Models.Category.list.GetListCResponse;
import akop.id.budgetplanner.Models.Transaction.Create.CreateTransaction;
import akop.id.budgetplanner.Models.Transaction.Create.CreateTransactionResponse;
import akop.id.budgetplanner.R;
import akop.id.budgetplanner.Services.APIClient;
import akop.id.budgetplanner.Services.GBPAPI;
import akop.id.budgetplanner.Views.BaseActivity;
import androidx.annotation.NonNull;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.blackcat.currencyedittext.CurrencyEditText;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class GBPActivityNewTransaction extends BaseActivity {
    /**
     * The multiple choice list dialog.
     */
    private CFAlertDialog alertDialog;

    @BindView(R.id.editTextTransactionType)
    TextInputEditText editTextType;
    @BindView(R.id.editTextTransactionCategory)
    TextInputEditText editTextCategory;
    @BindView(R.id.editTexttransactionAmount)
    CurrencyEditText editTextAmount;
    @BindView(R.id.editTexttransactionDescription)
    TextInputEditText editTextDescription;
    @BindView(R.id.buttonTransactionSubmit)
    Button buttonSubmit;

    private List<Integer> list = new ArrayList<>();

    private Integer selectedCategoryID;
    private Integer selectedTypeID = 1;
    private String token = Prefs.getString("token", "");
    private GBPAPI gbpapi = APIClient.getClient().create(GBPAPI.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gbpnew_transaction);
        ButterKnife.bind(this);

        setToolbar();
        setToolbarTitle("Transaction");
        showBackArrow();

        editTextAmount.setLocale(Utils.getLocale("IDR"));
        initializeMultipleChoiceListDialog();
        initializeCategoryTextField();
        initializeSubmitButton();
        showAlertDialog(editTextAmount.getRawValue()+"");

    }
    static class Utils {
        public static SortedMap<Currency, Locale> currencyLocaleMap;

        static {
            currencyLocaleMap = new TreeMap<Currency, Locale>(new Comparator<Currency>() {
                public int compare(Currency c1, Currency c2) {
                    return c1.getCurrencyCode().compareTo(c2.getCurrencyCode());
                }
            });
            for (Locale locale : Locale.getAvailableLocales()) {
                try {
                    Currency currency = Currency.getInstance(locale);
                    currencyLocaleMap.put(currency, locale);
                } catch (Exception e) {
                }
            }
        }
        static Locale getLocale(String currencyCode) {
            Currency currency = Currency.getInstance(currencyCode);
            return currencyLocaleMap.get(currency);
        }
        public static String getCurrencySymbol(String currencyCode) {
            Currency currency = Currency.getInstance(currencyCode);
            return currency.getSymbol(currencyLocaleMap.get(currency));
        }

    }
    /**
     *
     */
    private void initializeSubmitButton() {
       buttonSubmit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               CreateTransaction transaction = new CreateTransaction(
                       selectedCategoryID,
                       (int) editTextAmount.getRawValue(),
                       editTextDescription.getText().toString(),"True",
                       getIntent().getIntExtra("walletID", 0),
                       selectedTypeID,
                       new String[0]
               );
               Call<CreateTransactionResponse> transactionResponseCall = gbpapi.putTransaction(token,getIntent().getIntExtra("walletID", 0) ,transaction);
               transactionResponseCall.enqueue(new Callback<CreateTransactionResponse>() {
                   @Override
                   public void onResponse(Call<CreateTransactionResponse> call, Response<CreateTransactionResponse> response) {
                       if (response.isSuccessful()) {
                           finish();
                       }
                   }

                   @Override
                   public void onFailure(Call<CreateTransactionResponse> call, Throwable t) {

                   }
               });
           }
       });
    }

    /**
     * Initializes the multiple choice list dialog.
     */
    private void initializeMultipleChoiceListDialog() {
        editTextType.setClickable(true);
        editTextType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(GBPActivityNewTransaction.this);
                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET);
                builder.setTitle("Transaction");
                builder.setMessage("Select your transaction type");
                builder.addButton("Income", -1, -1, CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editTextType.setText("Income");
                        selectedTypeID = 2;
                        alertDialog.dismiss();
                    }
                });
                builder.addButton("Expense", -1, -1, CFAlertDialog.CFAlertActionStyle.NEGATIVE, CFAlertDialog.CFAlertActionAlignment.JUSTIFIED, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editTextType.setText("Expense");
                        selectedTypeID = 1;
                        alertDialog.dismiss();
                    }
                });
                builder.setCancelable(true);
                alertDialog = builder.show();
                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                    }
                });
            }
        });
    }

    private void initializeCategoryTextField() {

        int id = getIntent().getIntExtra("walletID", 0);

        Call<GetListCResponse> call = gbpapi.getCategoryList(token, id);
        call.enqueue(new Callback<GetListCResponse>() {
            @Override
            public void onResponse(Call<GetListCResponse> call, Response<GetListCResponse> response) {
                final String[] categories;
                if (response.isSuccessful()) {
                    categories = new String[response.body().getContent().size()];
                    for (int i = 0; i < response.body().getContent().size(); i++) {
                        categories[i] = response.body().getContent().get(i).getName();
                        list.add(response.body().getContent().get(i).getId());
                    }
                    editTextCategory.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                        @Override
                        public void onFocusChange(View view, boolean b) {
                            if (b) {
                                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(GBPActivityNewTransaction.this);
                                builder.setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT);
                                builder.setTitle("Select Transaction Category!");
                                builder.setItems(categories, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int index) {
                                        editTextCategory.setText(categories[index]);
                                        selectedCategoryID = list.get(index);
                                        dialogInterface.dismiss();
                                    }
                                });
                                builder.show();
                            }
                        }
                    });
                }
            }
            @Override
            public void onFailure(Call<GetListCResponse> call, Throwable t) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_attach, menu);

        // return true so that the menu pop up is opened
        return true;
    }
}
