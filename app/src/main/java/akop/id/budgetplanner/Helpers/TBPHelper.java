package akop.id.budgetplanner.Helpers;

import java.text.NumberFormat;
import java.util.Locale;

public class TBPHelper {


    public String convert(Integer val){

        Locale localeID = new Locale("in", "ID");

        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

        return formatRupiah.format((double)val);
    }
}
