package akop.id.budgetplanner.Helpers;

import java.text.DecimalFormat;

public class GBPHelper {

    public static String subsValue(String value){
        return value.substring(0, value
                .length() - 5);
    }

    public static String formatCurrency(String symbol ,String amount){
        return symbol + " " + currencyFormatter(subsValue(amount));
    }

    public static String currencyFormatter(String num) {
        double m = Double.parseDouble(num);
        DecimalFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(m);
    }

    public static String statusconfirmed(Boolean status){
        if (status){
            return "confirmed";
        } else {
            return "unconfirmed";
        }
    }
}
